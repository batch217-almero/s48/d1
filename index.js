// console.log("Test");

let posts = [];//storage for posts
let count = 1;//will serve as object id

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener('submit', (event) => {

	event.preventDefault(); //prevent page from refresh
	posts.push({
		id:count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})

	count++;//increment object id
	showPost(posts);
	alert("Successfully added");	
});

// SHOW POST
const showPost = (post) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
};



// EDIT POST: Locate post to be edited
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-title-edit").value = title;
	document.querySelector("#txt-body-edit").value = body;
}

// UPDATE POST
document.querySelector("#form-edit-post").addEventListener('submit', (event) => {
	event.preventDefault();

	for(let i = 0; i < posts.length; i++){
		if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector("#txt-title-edit").value;
			posts[i].body = document.querySelector("#txt-body-edit").value;

			showPost(posts);
			alert("Successfully Updated");

			break; //stop loop if matching post already found
		}


	}

});

// DELETE POST
const deletePost = (id) => {
	posts = posts.filter((post) => {
		if (post.id.toString() !== id) {
			return post;
		}
	})
document.querySelector(`#post-${id}`).remove();
}


